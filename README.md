# SRE CD POC

## intro

mini project how to work with functions and flask/gunicorn

## Install 

It's best to create venv for this, depending on your OS and local setup, this should work
`virtualenv -p python3 venv`

It will create venv directory! next step is to activate it 
`source venv/bin/activate`

Next step is to install requirements
`pip install -r requirements.txt`
## How to start it

The idea is to run web flask for develpment purpuses, ex:

`FLASK_DEBUG=development python wsgi.py`
will make flask run on `0.0.0.0`. 

For production, we would use gunicorn because it has workers and timeouts. ex:

`gunicorn --bind 0.0.0.0:5000 wsgi:application --timeout 10 --workers 4`