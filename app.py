from flask import Flask
from flask import jsonify
from functions.fibonacci import fibonacci
from functions.ackermann import ackermann
from functions.factorial import factorial
from statsd import StatsClient

statsd = StatsClient()
application = Flask('demo')


@application.route("/")
def root():
    return "demo app"


@statsd.timer('fibonacci')
@application.route("/v1/fibonacci/<path:number>")
def api_fibonacci(number):
    return str(fibonacci(int(number)))


@statsd.timer('ackermann')
@application.route("/v1/ackermann/<path:arg_1>/<path:arg_2>")
def api_ackermann(arg_1, arg_2):
    return str(ackermann(int(arg_1), int(arg_2)))


@statsd.timer('factorial')
@application.route("/v1/factorial/<path:number>")
def api_factorial(number):
    return str(factorial(int(number)))


@application.errorhandler(RecursionError)
def handle_invalid_usage(e):
    return f'Too much RecursionError, please adjust sys.getrecursionlimit() function. the error exception text is ' \
           f'"{e}" ', 500


if __name__ == "__main__":
    raise EnvironmentError('Don\'t run this directly, use wsgi.py')