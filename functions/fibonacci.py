def fibonacci(number):
    if number in [0, 1]:
        return number
    else:
        return fibonacci(number - 1) + fibonacci(number - 2)


def list_fibonacci():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b